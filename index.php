<?php
    require_once ('animal.php');
    require_once ('frog.php');
    require_once ('ape.php');

    $new_animal = new animal ("shaun");
    echo "name : " .$new_animal ->name. "<br>";
    echo "legs : " .$new_animal ->legs. "<br>";
    echo "cold blooded : " .$new_animal ->cold_blooded. "<br><br>";

    $kodok = new frog ("buduk");
    echo "name : " .$kodok ->name. "<br>";
    echo "legs : " .$kodok ->legs. "<br>";
    echo "cold blooded : " .$kodok ->cold_blooded. "<br>";
    echo $kodok ->jump(). "<br><br>";
    

    $sungokong = new ape ("kera sakti");
    echo "name : " .$sungokong ->name. "<br>";
    echo "legs : " .$sungokong ->legs. "<br>";
    echo "cold blooded : " .$sungokong ->cold_blooded. "<br>";
    echo $sungokong ->yell();

    


?>